# SignalPath

Platform Engineering Code Assessment

1. 
* JQuery    3
* Python    1
* PHP       1
* Scala     3
* Clojure   4
* Haskell   4
* Golang    4
* Node.js   4
* Ruby      4
* Perl      4
* Java      1
* Angular   3
* Chef      3
 


2. hex_base64.py is the python script to encode given hex number to base64.
In the output you'll see 2 values. One is the ouput when using Python codecs 
package and the other without that package.
I wasn't sure if I could use the Python package.


3. Files simpleMath.scala and simpleMathEfficient.scala are both for problem 
number 3 simpleMath.scala is a straight scala copy of the given python script.
However when I tried to get value for 50,55,60 I saw the time jumping from 
55sec to 11min to 119min. Since the algorithm was a modification of the 
fibonacci algorithm I did some research for a more efficient way and 
found the 'tail-recursion' method. After several failed 
attempts to modify the tail-recursive fibonacci went back to researching on the 
sequence and found that this was a A001924-OEIS sequence. Further exploration 
led to the following paper where I found a(n) = f(n+4)-3-n as the formula. 
http://www.tanyakhovanova.com/Sequences/CreatingNewSequences.html 
Applied that formula to the tail-recursive fibonacci algorithm to get the value 
for 90 as 19740274219868223074 and time taken was 3ms