import java.util.concurrent.TimeUnit
object simpleMath {
  def main(args: Array[String]): Unit = {
    println(time{simpleM(args(0).toInt)})
  }

  // Scala conversion of the simple Python script
  def simpleM(n:Int) :BigInt = n match{
    case 0 | 1 => n
    case _ => n + simpleM(n-1) + simpleM(n-2)
  }

  // Calculating elapsed time for the passed function.
  def time[R](block: => R): R = {
    val t0 = System.nanoTime()
    val result = block   
    val t1 = System.nanoTime()
    val r = TimeUnit.MINUTES.convert(t1-t0,TimeUnit.NANOSECONDS)
    println("Elapsed time: " + (t1 - t0) + " ns")
    println("Elapsed time: " + r + " min")
    result
  }
}