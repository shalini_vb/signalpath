import java.util.concurrent.TimeUnit
object simpleMathEfficient {
    def main(args: Array[String]): Unit = {
        println(time{fib(args(0).toInt)})
    }

    def fib(n:Int) :BigInt = {
        def fib_tail( n: Int, a:BigInt, b:BigInt): BigInt = n match {
            case 0 => a
            case _ => fib_tail( n-1, b, a+b )
        }
        return  fib_tail( n+4, 0, 1 )-n-3
    }
    // Calculating elapsed time for the passed function.
    def time[R](block: => R): R = {
        val t0 = System.nanoTime()
        val result = block    // call-by-name
        val t1 = System.nanoTime()
        val r = TimeUnit.MILLISECONDS.convert(t1-t0,TimeUnit.NANOSECONDS)
        println("Elapsed time: " + (t1 - t0) + "ns")
        println("Elapsed time: " + r + " ms")
        result
  }
}