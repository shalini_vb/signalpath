import sys
import string
import codecs

def hex_bin(hex_num):
    """Convert hex_num given to binary string"""
    bin_str=''
    for c in hex_num:
        c_to_bin = bin(int(c,16)).replace('0b','')
        zero_pre = 4 - len(c_to_bin)
        for x in range(0, zero_pre):
            c_to_bin = '0' + c_to_bin
        bin_str += c_to_bin
    return bin_str

def append_zeros(hex_num, divisor):
    """Return 0x padding for hexadecimal number given. """
    by_how_many = len(hex_num) % divisor
    zero = ''
    if by_how_many == 0: return zero
    for x in range(0 , divisor-by_how_many):
        zero += '0'
    return zero

def append_equals(base64_num):
    """Return ='s padding for the base64 string given."""
    by_how_many = len(base64_num) % 4
    equals = ''
    if by_how_many == 0: return equals
    for x in range(0 , 4-by_how_many):
        equals += '='
    return equals

def main():

    if len(sys.argv) != 2:
        print("Please enter one Hexadecimal number.")
        exit()

    # Store the base64 encoding for each character.
    # String index represents the encoded value
    base64_encoding = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

    # Only encode if given number is hexadecimal
    if all(c in string.hexdigits for c in sys.argv[1]):
        hex_num = sys.argv[1].strip()
        #b64 = base64.b64encode(bytes.fromhex(hex_num)).decode

        # Converting using codecs python package
        # length of hex number must be even numbered to so pad with 0x
        hex2 = hex_num + append_zeros(hex_num,2)
        b64 = codecs.encode(codecs.decode(hex2, 'hex'), 'base64').decode()
        print("Base64 encoded using Python codecs package: "+b64)
        
        # Encoding hex to base64 without codecs
        # Get binary string of hex 
        binary_string = hex_bin(hex2)
        #pad with 0's to get groups of 6
        binary_string += append_zeros(binary_string,6)
        
        base64_encoded = ''
        x = 0
        while x < len(binary_string) :
            # get array index by evaluating integer value of every 6 bit group
            base64_encoded += base64_encoding[int(binary_string[x:x+6],2)]
            x+=6
        print("Base64 encoded without codecs: "+base64_encoded+append_equals(base64_encoded))
    else:
        print("Given number is not a hexadecimal.")
        exit()

if __name__ == '__main__':
    main()
